/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

$( document ).ready(function() {
    $(window).on('load', function(){
      $("#toolbar-bar").mCustomScrollbar({
        theme:"minimal"
      });
    });
    /**
     * Menu
    */
    $( ".menu-ico" ).on( "click", function(event) {
      $("body").toggleClass("menu-open");
    });

    /**
     * Search
    */
    if($('#search-block-form').length) {
      $('#search-block-form .form-text').attr('placeholder', 'Type anywhere to search');
      $('#search-block-form').prepend('<i aria-hidden="true" class="fa fa-times"></i>');
      $('.search-bar, .search-block-form .fa').click(function() {
        $('body').toggleClass('sch-act');
      });
    } else {
      $('body').addClass('sch-dis');
    }

    $('.toolbar-tray-horizontal li.menu-item--expanded > a').on('click', function(event){
        event.preventDefault();
        var element = $(this).parent('li');
        if (element.hasClass('menu-expand')) {
          element.removeClass('menu-expand');
          element.find('li').removeClass('menu-expand');
        }
        else {
          element.addClass('menu-expand');
          element.siblings('li').removeClass('menu-expand');
          element.siblings('li').find('li').removeClass('menu-expand');
        }
    });
    
    $(".toolbar-tray-horizontal").delegate("li.menu-item--expanded > a", "dblclick", function(e) {
      window.location.href = jQuery(this).attr('href');
    });

    /*
    ** On mouseover
    */
    $("#toolbar-administration").hover(
      function () {
        $('body').addClass("mnu_hover");
      },
      function () {
        $('body').removeClass("mnu_hover");
      }
    );
	});

})(jQuery, Drupal, this, this.document);
