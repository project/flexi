CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Flexi is a Mobile-first responsive layout in Drupal 8 Base theme and it is
fully themed with Drupal Components like [ Blocks, Forms, Tabs, etc.], most
flexible and user-friendly theme for backend and frontend.


INSTALLATION
------------

1. Download Flexi from https://www.drupal.org/project/Flexi/

2. Unpack the downloaded file and and place it in your Drupal installation
   instance at the file path under /themes.

   Additional installation folders can be used, for more information
   see https://www.drupal.org/docs/8/extending-drupal-8/installing-themes


CONFIGURATION
-------------

1. Log in as an administrator on your Drupal 8 site and move to Appearance
   page at the path admin/appearance. You will see the Flexi theme listed under
   the Disabled Themes heading. You can select Flexi as an administration theme
   and default theme.

2. Now build your own sub-theme set Flexi as the Base Theme.

Admin Menu
----------

Admin menu orientation has been changed from landscape to portrait.


Font Awesome
------------

Font Awesome is used for icons.
http://fontawesome.io/


SASS
----

Flexi is a Drupal theme and it can be compiled using CSS Preprocessor SASS.
You must have SASS setup first in your project.
Read more in http://sass-lang.com/


MAINTAINERS
-----------

Sakthivel M (sakthivel-m)(https://www.drupal.org/u/sakthivel-m)
